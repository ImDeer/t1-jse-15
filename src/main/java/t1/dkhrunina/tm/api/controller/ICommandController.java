package t1.dkhrunina.tm.api.controller;

public interface ICommandController {

    void showAbout();

    void showArgumentError();

    void showCommandError();

    void showHelp();

    void showSystemInfo();

    void showVersion();

}